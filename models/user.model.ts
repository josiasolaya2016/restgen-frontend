export interface UserEntity {
    username: string;
    name: string;
    access_token: string;
}

export interface UserLogin {
    email?: string;
    password?: string;
}

export interface UserSignUp {
    username: string;
    name: string;
    lastName: string;
    password: string;
    email: string;
}