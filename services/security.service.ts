import { UserEntity } from '../models/user.model';

export class ServiceSecurity {
    url: string = 'http://192.168.100.4:3333/';
    user: UserEntity = {
        username: '',
        name: '',
        access_token: ''
    };
    constructor() {
        
    }

    setUser(_user: UserEntity) {
        this.user = _user;
    }
}