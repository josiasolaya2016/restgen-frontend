"use client";
import Image from "next/image";
import { Button, Card, CardBody } from "@nextui-org/react";
import NavBar from "../../../components/NavBar";
import Link from "next/link";
import { useCallback } from "react";
import  Particle from "../../../components/Particle";
import { ServiceSecurity } from "../../../services/security.service";
import { useRouter } from 'next/navigation'

export default function ProjectGenerated(params) {
    const username = "username";
    const button = username ? username : "Log in";
    const buttonSign = username ? "Sign out" : "Sign up";
    const securityService: ServiceSecurity = new ServiceSecurity();
    const models = Number(params.searchParams.models);
    async function downloadFile() {
        console.log(params.searchParams.id, params);
        const id = params.searchParams.id;
        const obj = {
            'project': id
        };
        const response = await fetch(securityService.url + 'project/download', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
              },
            body: JSON.stringify(obj),
        });
        const blob = await response.blob();
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.download = 'archive.zip';
        link.click();
        window.URL.revokeObjectURL(url);
        // console.log('es el  post services', await response.json());
    }

    async function deployFile() {
        console.log(params.searchParams.id, params);
        const id = params.searchParams.id;
        const obj = {
            'project': id
        };
        const response = await fetch(securityService.url + 'project/deploy', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
              },
            body: JSON.stringify(obj),
        });
        const blob = await response;
        console.log(blob);
        // console.log('es el  post services', await response.json());
    }

    return (
    
        <div className="flex flex-row h-full">
          <NavBar loginButton={button}  signButton={buttonSign}/>

          <div className="bg-transparent flex-col flex md:flex-row sm:flex-col h-200 items-center mx-auto my-auto w-4/5 justify-center">
            <div className="w-4/5">
                <div className="flex flex-row items-center justify-center mb-2">
                    <Button className="font-bold mx-auto text-white bg-black w-40 ">{ username }</Button>
                    <span className="text-2xl"> / </span>
                    <Button className="font-bold mx-auto text-white bg-black w-40" onClick={downloadFile}>Download</Button>
                    <span className="text-2xl"> / </span>
                    <Button className="font-bold mx-auto text-white bg-black w-40">Proyecto 1</Button>

                </div>
                <div className="flex flex-col w-full bg-white rounded-lg">
                    <div className="flex flex-row justify-evenly w-full rounded-lg">
                        <div className="h-40 w-full flex flex-col bg-white justify-center items-center text-center rounded-lg">
                            <span className="font-bold text-black">Generated models</span>
                            <span className="font-bold text-black">{models}</span>
                        </div>
                        <div className="h-40 w-2 bg-black rounded-lg"></div>
                        <div className="h-40 w-full flex flex-col bg-white justify-center items-center text-center rounded-lg">
                            <span className="font-bold text-black">Generated controllers</span>
                            <span className="font-bold text-black">{models}</span>
                        </div>
                    </div>
                    <div className="w-full h-1 bg-black rounded-lg"></div>
                    <div className="flex flex-row justify-between w-full justify-center rounded-lg">
                        <div className="h-40 w-full flex flex-col bg-white justify-center items-center text-center rounded-lg">
                            <span className="font-bold text-black">Generated services</span>
                            <span className="font-bold text-black">{models}</span>
                        </div>
                        <div className="h-40 w-2 bg-black rounded-lg"></div>
                        <div className="h-40 w-full flex flex-col bg-white justify-center items-center text-center rounded-lg">
                            <span className="font-bold text-black">Generated relations</span>
                            <span className="font-bold text-black">{models/2}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="w-2/5 flex items-center justify-center flex-col">
                <Button className="font-bold mx-auto  w-40 " onClick={deployFile}>Deploy</Button>
                <Image
                alt="Card background"
                className="object-cover rounded-xl mx-auto mb-3"
                src="/graphs.png"
                width={142}
                height={142}
                />
            </div>
          </div>
        </div>
      );
}

