"use client";
import Image from "next/image";
import { Button, Card, CardBody } from "@nextui-org/react";
import NavBar from "../../components/NavBar";
import Link from "next/link";
import { useCallback, useRef, useState } from "react";
import Particle from "../../components/Particle";
import { ServiceSecurity } from "../../services/security.service";
import { redirect } from "next/navigation";
// useRouter
import { useRouter } from "next/navigation";

export default function Home() {
  const username = "username";
    const button = username ? username : "Log in";
    const buttonSign = username ? "Sign out" : "Sign up";
  const [dragActive, setDragActive] = useState<boolean>(false);
  const inputRef = useRef<any>(null);
  const [files, setFiles] = useState<any>([]);
  const router = useRouter();
  const securityService: ServiceSecurity = new ServiceSecurity();
  console.log(securityService.user);
  
  function handleChange(e: any) {
    e.preventDefault();
    if (e.target.files && e.target.files[0]) {
      for (let i = 0; i < e.target.files["length"]; i++) {
        setFiles([e.target.files[i]]);
        handleSubmitFile(e.target.files[i]);
      }
    }
  }

  async function handleSubmitFile(archivos: any) {
    if (archivos instanceof Blob) {
      if (archivos.length === 0) {
        console.log(archivos, "no hay nad");

        // no file has been submitted
      } else {
        const user = 1;
        const formData = new FormData();
        formData.append("file", archivos);
        formData.append("user", user.toString());
        console.log(archivos);
        const response = await fetch(securityService.url + "project/upload", {
          method: "POST",
          body: formData,
        });
        const responseJSON = await response.json();
        console.log("es el  post services", responseJSON);
        router.push(
          `/project?id=${responseJSON.id}&models=${responseJSON.models}`,
          { scroll: false }
        );
        // write submit logic here
      }
    }
  }

  function handleDrop(e: any) {
    e.preventDefault();
    e.stopPropagation();
    setDragActive(false);
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      for (let i = 0; i < e.dataTransfer.files["length"]; i++) {
        setFiles([e.dataTransfer.files[i]]);
        handleSubmitFile(e.dataTransfer.files[i]);
      }
    }
    handleSubmitFile(e);
  }

  function handleDragLeave(e: any) {
    e.preventDefault();
    e.stopPropagation();
    setDragActive(false);
  }

  function handleDragOver(e: any) {
    e.preventDefault();
    e.stopPropagation();
    setDragActive(true);
  }

  function handleDragEnter(e: any) {
    e.preventDefault();
    e.stopPropagation();
    setDragActive(true);
  }

  function removeFile(fileName: any, idx: any) {
    const newArr = [...files];
    newArr.splice(idx, 1);
    setFiles([]);
    setFiles(newArr);
  }

  function openFileExplorer() {
    inputRef.current.value = "";
    inputRef.current.click();
  }

  return (
    <div className="flex flex-row h-full">
      <Particle />
      <NavBar loginButton={button}  signButton={buttonSign}/>
      <div className="bg-transparent h-200 items-center mx-auto my-auto justify-center">
        <div className="mb-10 flex justify-center text-left">
          <div className="w-35">
            <h1 className="font-bold text-4xl text-white">RestGen</h1>
            <h3 className="text-white">be free.</h3>
          </div>
        </div>

        <Card className="h-full bg-transparent border-2 w-300">
          <CardBody className="-p-5 bg-transparent justify-center text-center flex flex-col w-full ">
            {files.length === 0 ? (
              <>
                <form
                  className={`${
                    dragActive ? "bg-gray-800" : "transparent"
                  }  w-300 rounded-lg p-5 h-full text-center flex flex-col items-center justify-center`}
                  onDragEnter={handleDragEnter}
                  onSubmit={(e) => e.preventDefault()}
                  onDrop={handleDrop}
                  onDragLeave={handleDragLeave}
                  onDragOver={handleDragOver}
                >
                  <Image
                    alt="Card background"
                    className="object-cover rounded-xl mx-auto mb-3"
                    src="/upload_doc.svg"
                    width={72}
                    height={72}
                  />
                  <input
                    placeholder="fileInput"
                    className="hidden"
                    ref={inputRef}
                    type="file"
                    multiple={false}
                    onChange={handleChange}
                    accept=".sql"
                  />
                  <Button
                    className="font-bold mx-auto bg-white w-40 mb-2"
                    onClick={openFileExplorer}
                  >
                    Upload file
                  </Button>
                  <Link href="#pastecode" className="text-white">
                    or paste text
                  </Link>
                </form>
              </>
            ) : (
              <div className="p-10 mx-10">
                <Image
                  alt="Card background"
                  className="object-cover rounded-xl mx-auto mb-3"
                  src="/upload_doc.svg"
                  width={72}
                  height={72}
                />
                <span className="font-bold text-white">Loading...</span>
              </div>
            )}
          </CardBody>
        </Card>
      </div>
    </div>
  );
}
